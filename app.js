var http = require('http'),
	express = require('express'),
	path = require('path'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	flash = require('connect-flash'),
	session = require('express-session'),
	q = require('q'),
	handlebars = require('express-handlebars'),
	handlebars_sections = require('express-handlebars-sections'),
	morgan = require('morgan'),
	CONFIG = require('./config'),
	helpers = require('./app/helpers'),
	routes = require('./app/routes');


var app = express();

//----------------mongo db----------------
var User = require('./app/models/User'),
	Category = require('./app/models/Category'),
	Post = require('./app/models/Post'),
	Comment = require('./app/models/Comment');
mongoose.Promise = q.Promise;
mongoose.connect(CONFIG.DB.uri, {
		useMongoClient: true,
		reconnectTries: 30,
		reconnectInterval: 1000,
		keepAlive: 1,
		connectTimeoutMS: 30000
	}).then(() => {
		debugger
		console.log('SUCCESS MGDB');
		helpers.initDB().then((rlt) => {
			if (rlt) console.log('INIT DB SUCCESS!');
			else console.log('DO NOT INIT DB, NOT FIRST TIME!');
			return helpers.get_categoryTree();
		}).then((category_tree) => {
			var estate_cats;
			category_tree.forEach((branch, i) => {
				if (branch.parent.pathname == 'nha-dat') {
					estate_cats = category_tree[i].childs;
				} else news_cat = category_tree[i].parent;
			});
			app.locals.DATA = {
				cats: estate_cats,
				news_cat: news_cat
			};
		}).catch((err) => {
			console.log('INIT DATA ERROR');
			console.log(err);
		})
	})
	.catch((err) => {
		console.log(err);
	});

//----------------configurations----------------
helpers.config_passport(passport); // pass passport for configuration

app.set('port', process.env.PORT || 8000);

//set views folder //default: views --> can omit
app.set('views', path.join(__dirname, '/app/views'));

//set view engine
app.engine('hbs', handlebars({
	extname: 'hbs',
	defaultLayout: 'main',
	layoutsDir: path.join(__dirname, '/app/views/_layouts/'),
	partialsDir: path.join(__dirname, '/app/views/_partials/'),
	helpers: {
		section: handlebars_sections(),
		formatCurrency: function(n) {
			return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + "$";
		},
		convertPostTitleAndIdToPath: function(title, id) {
			return helpers.StringFormat.convertPostTitleAndIdToPath(title, id);
		}
	}
}))
app.set('view engine', 'hbs');

//----------------middlewares------------------
//static resources
app.use(express.static(path.join(__dirname, '/app/public')));
//logger
app.use(morgan('short'));
//body parser
app.use(bodyParser.urlencoded({
	extended: false
}), bodyParser.json());
//session
app.use(session({
	key: 'session_cookie_name',
	secret: 'session_cookie_secret',
	resave: false,
	saveUninitialized: false
}));
//passport
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
//flash
app.use(flash());

//----------------routers----------------
app.use((req, res, next) => {
	req.APPDATA = app.locals.DATA;
	next();
});
app.use(routes);

//----------------Starting the App----------------
var server = http.createServer(app);
var boot = function() {
	server.listen(app.get('port'), function() {
		console.info('Express server listening on port ' + app.get('port'));
	});
};
var shutdown = function() {
	server.close();
};
if (require.main === module) {
	boot();
} else {
	console.info('Running app as a module');
	exports.boot = boot;
	exports.shutdown = shutdown;
	exports.port = app.get('port');
}
var count = 0;
const MAX = 6;

$(document).ready(function() {
    // var div = createImageInputFormGroup();
    // $('.image-input-list').append(div);
});

function createPostHandle() {
    var box = $("#messageModal");
    var title = $('input[name=title]')[0].value;

    if (title.length > 3) {
        var form = $("#form-create-post");
        tinyMCE.triggerSave();

        $.post(form.attr('action'), form.serialize(), function(data) {

            if (data.success == true) {
                box.find('p').text('Tạo bài đăng thành công');

            } else {
                box.find('p').text(data.msg);
            }
            box.modal();
        }, 'json');
    } else {
        box.find('p').text('Thoông tin không hợp lệ!');
        box.modal();
    }
}

$('#create-post-btn').on('keypress', function(e) {
    if (e.which == 13) {
        createPostHandle();
        return false;
    }
});
$("#create-post-btn").on('click', function() {
    createPostHandle();
});

// function createImageInputFormGroup() {
//     var id = "image" + (count + 1);
//     var div = $('<div class="form-group col-sm-4 col-xs-4 text-center input-image-wrappwer"></div>');
//     var image_input = $('<input class="input-image-upload empty" type="file" name="' + id + '" id="' + id + '" accept="image/*">');
//     var preview = $('<div><label for="' + id + '" class="preview"><img class="img-thumbnail" src="/images/placeholder.png"></label></div>');
//     div.append(image_input);
//     div.append(preview);

//     image_input.change(handleImageInputChange);

//     return div;
// }
// var handleImageInputChange = function() {
//     var div = $(this).parent();
//     var image_preview = div.find('img');
//     var image_input = div.find('input');

//     if (image_input.prop('files')[0]) {
//         // Use FileReader to get file
//         var reader = new FileReader();

//         //image has loaded --> preview
//         reader.onload = function(e) {
//             image_preview.attr('src', e.target.result);
//             if (div.find('.full').length > 0) return;

//             count++;
//             image_input.removeClass('empty').addClass('full');
//             var btn_remove = $('<button type="button" class="btn-remove">Remove</button>');
//             div.append(btn_remove);

//             btn_remove.click(function() {
//                 var parent = div.parent();
//                 div.remove();
//                 count--;

//                 parent.find('.input-image-wrappwer').each(function(index) {
//                     var id = 'image' + (index + 1);
//                     $(this).find('input').attr('name', id);
//                     $(this).find('input').attr('id', id);
//                     $(this).find('label').attr('for', id);
//                 });

//                 if (count == (MAX - 1)) {
//                     var new_input_group = createImageInputFormGroup();
//                     $('.image-input-list').append(new_input_group);
//                 }
//             });

//             if (count < MAX) {
//                 var new_input_group = createImageInputFormGroup();
//                 $('.image-input-list').append(new_input_group);
//             }
//         }

//         // Load image
//         reader.readAsDataURL($(this).prop('files')[0]);
//     }
// }
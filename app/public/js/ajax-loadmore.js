var NUM_POSTS_PER_PAGE = 2;
// Biến dùng kiểm tra nếu đang gửi ajax thì ko thực hiện gửi thêm
var is_busy = false;

// Biến lưu trữ trang hiện tại
var page = 0;

// Biến lưu trữ rạng thái phân trang 
var stopped = false;

$(document).ready(function() {
    $('.load-more').trigger('click');
});

// Khi load-more click thì xử lý
$('.load-more').on('click', function() {
    debugger

    // Element append nội dung
    $element = $('.list-posts');

    // ELement hiển thị chữ loadding
    $loadding = $('#loadding');

    // Nếu đang gửi ajax thì ngưng
    if (is_busy == true) {
        return false;
    }

    // Nếu hết dữ liệu thì ngưng
    if (stopped == true) {
        return false;
    }

    // Thiết lập đang gửi ajax
    is_busy = true;

    // Tăng số trang lên 1
    page++;

    // Hiển thị loadding
    $loadding.removeClass('hidden');

    // Gửi Ajax
    debugger
    var url, querystring = getParameterByName('q', window.location.href);
    if (window.location.pathname !== '/tim-kiem')
        url = '/ajax/get-posts-by-category/' + $('.list-posts').data('category-pathname') + '?page=' + page;
    else url = '/ajax/get-posts-by-querystring?q=' + querystring + '&page=' + page;

    $.get(url, function(data, status) {

        var posts = data.data;

        if (posts.length <= NUM_POSTS_PER_PAGE) {
            stopped = true;
            $('.load-more').hide();
        } else posts.pop();


        posts.forEach(function(post) {
            $element.append(createOneCellPost(post));
        });
        $('.span-date').each(function() {
            var date = new Date($(this).text());
            $(this).text(date.displayString());
            $(this).removeClass('span-date');
        });



        $loadding.addClass('hidden');
        is_busy = false;

    }, 'json');
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function createOneCellPost(post) {
    var cat_url = '/post/' + post.category.pathname;
    var url = '/post/' + post.category.pathname + '/' + post.path;
    if (post.images.length == 1) {
        post.images.push('undefined')
    }
    return $('<div class="row mybox white-bg mar-10 padd-10"><div class="media"><div class="media-left"><a class="" href="' + url +
        '"><img class="media-object img-responsive" src="' + post.images[1] + '" alt="Image not found" onerror="this.onerror=null;this.src=\'' + post.images[0] + '\';" ></a></div><div class="media-body"><h4 class="media-heading"><a href="' + url +
        '">' + post.title + '</a></h4><small><span class="pull-left"><i class="fa fa-clock-o"></i> <span class="span-date">' + post.createDate +
        '</span></span><span class="pull-right"><i class="fa fa-list-ul" aria-hidden="true"></i><a href="/post/' + post.category.pathname +
        '">' + post.category.name + '</a></span></small><div class="content">' + post.shortDesc +
        '</div><div class="span-link"><a href="' + url + '">Xem chi tiết</a></div><ul class="list-inline list-unstyled media-bottom"><li><small><i class="fa fa-comment-o"></i> ' + post.totalComments + ' comments</small></li></ul></div></div></div>');
}
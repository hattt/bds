$(document).ready(function() {
	Date.prototype.displayString = function() {
		var year = this.getFullYear(),
			month = this.getMonth() + 1,
			day = this.getDate(),
			hour = this.getHours(),
			min = this.getMinutes(),
			sec = this.getSeconds();

		var month = (month > 9 ? '' : '0') + month,
			day = (day > 9 ? '' : '0') + day,
			hour = (hour > 9 ? '' : '0') + hour,
			min = (min > 9 ? '' : '0') + min,
			sec = (sec > 9 ? '' : '0') + sec;

		return [hour, min, sec].join(':') + ' ' + [day, month, year].join('/');
	};
	editDateSpan();
	var msgBox = $('#messageModal');
	if ($(msgBox).find('#has-message').length != 0)
		msgBox.modal('show');

	var url = window.location.href;
	var zalo_btn = $('<div class="social-btn zalo-share-button" data-href="' + url + '" data-oaid="4255700735061628047" data-layout="4" data-color="blue" data-customize=false></div>')
	$('.social-box').append(zalo_btn);
	$.getScript("https://sp.zalo.me/plugins/sdk.js", function() {});

});

function editDateSpan() {
	$('.span-date').each(function() {
		var date = new Date($(this).text());
		$(this).text(date.displayString());
		$(this).removeClass('span-date');
	});
}

function showMessage(msg) {
	var box = $("#messageModal");
	box.find('p').text(msg);
	box.modal();
};

$('#messageModal').on('hidden.bs.modal', function() {
	location.reload();
});
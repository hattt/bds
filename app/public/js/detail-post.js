var postId;
$(document).ready(function() {
	debugger
	postId = $('#postId').text();
	loadComments(null, 1, $('.comments-list'), false, (function(rlt) {
		//if (rlt) $('.view-more-replies').trigger('click');
	}));
});

function createOneCommentCell(comment) {
	debugger
	if (comment.num_replies > 0)
		x = $('<li data-id="' + comment._id.toString() +
			'" class="comment"> <a class="pull-left" href="#"> ' +
			'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
			'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">' + comment.by + '</h4> <h5 class="time span-date">' + comment.at + '</h5> </div> <p>' +
			comment.text.toString() + '</p> <span class="span-link show-reply-box"><small>Trả lời</small></span>' +
			'<div> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i><small> ' + comment.num_replies.toString() + ' câu trả lời</small></span> </div> <ul class="comments-list"></ul><div class="reply-box"> </div> </div> </li>');
	else
		x = $('<li data-id="' + comment._id.toString() +
			'" class="comment"> <a class="pull-left" href="#"> ' +
			'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
			'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">' + comment.by + '</h4> <h5 class="time span-date">' + comment.at + '</h5> </div> <p>' +
			comment.text.toString() + '</p> <span class="span-link show-reply-box"><small>Trả lời</small></span>' +
			'<div hidden> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i>replies</span></div>' +
			'<ul class="comments-list"></ul><div class="reply-box"></div></div> </li>');

	return x;
}

function loadComments(parent, page, element, scroll, cb) {
	var url = '/ajax/get-comments/' + postId + '?' + (parent ? ('parent=' + parent) : '') +
		'&' + (page ? ('page=' + page) : '');
	$.getJSON(url, function(data) {
		if (!data.success) return cb(false);

		var comments = data.data;
		var x;
		comments.forEach(function(comment) {
			x = createOneCommentCell(comment);
			element.append(x);
		});
		editDateSpan();
		if (scroll) {
			var scrollTo = $(x);

			$('html, body').animate({
				scrollTop: scrollTo.offset().top - ($(window).height() / 2)
			}, 1000);
		}
		cb(true);
	});
}

function isScrolledIntoView(elem) {
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();

	var elemTop = $(elem).offset().top;
	var elemBottom = elemTop + $(elem).height();

	return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
$('.comments-list').on('click', '.show-reply-box', function() {
	var commentId = $(this).closest('li').data('id').toString();
	var replyBox = $(this).closest('li').find('.reply-box').last();
	if ($(replyBox).children().length <= 0) {
		var form = $('<form class="form-reply" action="/ajax/post-comment/' + postId + '" method="post"> <input type="text" hidden name="parent" value="' + commentId + '"><div class="input-group"><input class="form-control" placeholder="Nhập bình luận (ít nhất 3 kí tự)..." type="text" minlength="3" name="text"> <span class="input-group-addon  span-link send-reply"> <i class="fa fa-send"></i> </span> </div> </form>');
		replyBox.append(form);
	}
	debugger
	//scroll if not visible
	if (!isScrolledIntoView(replyBox)) {
		$('html, body').animate({
			scrollTop: $(replyBox).children().offset().top - ($(window).height() / 2)
		}, 1000);
	}
	replyBox.find('input').trigger('focus');
});
$('.comments-list').on('click', '.view-more-replies', function() {
	debugger
	var ul = $(this).closest('li').find('ul').first();
	ul.empty();
	$(this).hide();
	loadComments($(this).closest('li').data('id'), 1, ul, true, (function(rlt) {
		//if (rlt) $('.view-more-replies').trigger('click');
	}));
});

//send comment: click & enter
function sendCommentHandle(form, cb) {
	debugger
	$.post($(form).attr('action'), $(form).serialize(), function(data) {
		if (!data.success) {
			showMessage(data.msg || 'send comment false');
			return cb(false);
		}

		var ul = $('.comments-list').first(),
			comment = data.data;

		var x = createOneCommentCell(comment);
		ul.append(x);
		editDateSpan();

		var container = $('body'),
			scrollTo = $(x);

		container.animate({
			scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
		});
		cb(true);
	}, 'json');
}
$('.send-comment').on('click', function() {

	sendCommentHandle($(this).closest('form'));
});
$('#form-comment').keypress(function(e) {
	if (e.which == 13) {
		sendCommentHandle($(this));
		return false;
	}
});

//send reply: click & enter
function sendReplyHandle(form, cb) {
	var viewmore = form.closest('li').find('.view-more-replies').first();

	$.post($(form).attr('action'), $(form).serialize(), function(data) {
		if (!data.success) {
			showMessage(data.msg || 'send comment false');
			return cb(false);
		}

		$(viewmore).trigger('click');

		cb(true);
	}, 'json');
}
$('.comments-list').on('click', '.send-reply', function() {
	sendReplyHandle($(this).closest('form'));
});
$('.comments-list').on('keypress', '.form-reply', function(e) {
	if (e.which == 13) {
		sendReplyHandle($(this));
		return false;
	}
});
$(document).ready(function() {
	$.getScript("https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js", function() {
		//valdator config
		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			}
		});

		jQuery.validator.addMethod("noEmpty", function(value, element) {
			return value.trim();
		}, "Please not empty!");

		//validate form
		$("#form-register").validate({
			rules: {
				name: {
					required: true,
					noEmpty: true,
					maxlength: 50,
				},
				email: {
					required: true,
					email: true,
					maxlength: 100,
				},
				password: {
					required: true,
					minlength: 6,
					maxlength: 50,
				}

			},
			messages: {}
		});
		$("#form-login").validate({
			error: function(label) {
				$(this).addClass("error");
			},
			rules: {
				email: {
					required: true,
					email: true,
					maxlength: 100,
				},
				password: {
					required: true,
					minlength: 6,
					maxlength: 50,
				},

			},
			messages: {}
		});
	});
});

function registerHandle() {
	var form = $("#form-register");

	if (form.valid()) {
		$.post(form.attr('action'), form.serialize(), function(data) {
			if (data.success == true) {
				$("#registerModal").modal('hide');
				var box = $("#messageModal");
				box.find('p').text('Tạo tài khoản thành công');
				box.modal();

			} else {
				$("#registerErrorMsg").fadeOut(function() {
					$(this).text(data.msg).fadeIn();
				});
				// $("#registerErrorMsg").text(data.msg).fadeIn();
				// // grecaptcha.reset();
			}
		}, 'json');
	}

}

function loginHandle() {
	var form = $("#form-login");

	if (form.valid()) {
		$.post(form.attr('action'), form.serialize(), function(data) {

			if (data.success == true) {
				$("#loginModal").modal('hide');
				location.reload();

			} else {
				$("#loginErrorMsg").fadeOut(function() {
					$(this).text(data.msg).fadeIn();
				});
			}
		}, 'json');
	}

}
$('#form-register button').on('keypress', function(e) {
	if (e.which == 13) {
		registerHandle();
		return false;
	}
});
$('#form-login button').on('keypress', function(e) {
	if (e.which == 13) {
		loginHandle();
		return false;
	}
});
$("#form-register").on('click', 'button', function() {
	registerHandle();
});
$("#form-login").on('click', 'button', function() {
	loginHandle();
});
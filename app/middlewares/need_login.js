// route middleware to make sure a user is logged in
module.exports = (req, res, next) => {

	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();
	// if they aren't redirect them to the home page
	res.json({
		success: false,
		msg: 'Cần đăng nhập!'
	});
}
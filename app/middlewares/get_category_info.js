var Category = require('../models/Category'),
	CONFIG = require('../../config'),
	cats = CONFIG.POST_CATEGORIES;

//Add req.category (Category model prop + isEstate prop)
module.exports = (req, res, next) => {
	Category.findOne({
		pathname: req.params.category
	}).then((cat) => {
		if (!cat) throw new Error('404');
		return [cat, Category.findOne({
			pathname: 'nha-dat'
		})];
	}).spread((cat, estate_cat) => {
		req.category = cat;
		// req.category.isEstate = false;

		// if (cat.id == estate_cat.id) req.category.isEstate = true;
		// if (cat.parent) {
		// 	if (cat.parent.toString() == estate_cat.id) req.category.isEstate = true;
		// }
		next();
	}).catch((err) => {
		next(err);
	});
};
var q = require('q');
var Category = require('../models/Category'),
	Post = require('../models/Post');

module.exports = function(req, res, next) {
	res.locals.layoutVM = {};

	res.locals.layoutVM.cats = req.APPDATA.cats;
	res.locals.layoutVM.message = req.flash('message');
	res.locals.layoutVM.showLike = true;

	res.locals.layoutVM.loggedIn = req.isAuthenticated() ? true : false;
	if (res.locals.layoutVM.loggedIn) {
		var account = req.user.local || req.user.facebook || req.user.google;
		res.locals.layoutVM.name = account.name;
		res.locals.layoutVM.isAdmin = req.user.admin;
	}


	var promises = [
		Category.findOne({
			pathname: 'tin-tuc'
		})
	];

	q.all(promises).spread((newsCat) => {
		return q.all([Post.getAllPostsByCategory_SortByDate(newsCat, 1)]);
	}).spread((newsPosts) => {
		res.locals.layoutVM.newsPosts = newsPosts;
		return next();
	}).catch((err) => {
		next(err);
	});


}
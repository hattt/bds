module.exports = (req, res, next) => {
	req.session.prevPath = req.originalUrl;
	next();
}
module.exports = (err, req, res, next) => {
	var code = parseInt(err.message);
	var vm = {
		title: 'Lỗi',
		msg: 'Có lỗi xảy ra. Vui lòng thử lại sau!'
	};

	if (!code) code = 500;
	if (code == 404) {
		vm.title = 'Không tồn tại';
		vm.msg = 'Trang bạn yêu cầu không tồn tại!';
	}

	if (process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'test') vm.msg = err.stack;
	return res.status(code).render('error', vm);
};
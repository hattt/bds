module.exports = {
	get_a_post_by_path: require('./get_a_post_by_path'),
	error_handle: require('./error_handle'),
	layout_handle: require('./layout_handle'),
	save_prevPath: require('./save_prevPath'),
	hide_likeBox: require('./hide_likeBox'),
	get_category_info: require('./get_category_info'),
	need_login: require('./need_login'),
	need_admin: require('./need_admin')
};
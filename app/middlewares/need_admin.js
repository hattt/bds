// route middleware to make sure a user is logged in
module.exports = (req, res, next) => {

	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated()) {
		if (req.user.admin)
			return next();
	}

	req.flash('message', 'Cần quyền admin!');
	// if they aren't redirect them to the home page
	res.redirect(req.session.prevPath || '/');
}
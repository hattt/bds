var Post = require('../models/Post'),
	helpers = require('../helpers'),
	CONFIG = require('../../config');
//Add req.post

module.exports = (req, res, next) => {
	req.post = {};
	var id = helpers.StringFormat.getPostIdFromPath(req.params.name);
	Post.findOne({
		_id: id
	}).populate('category').then((post) => {
		if (!post) throw new Error('404');
		req.post = post;
		next();
	}).catch((err) => {
		next(err);
	});
};
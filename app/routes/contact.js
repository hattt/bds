var r = require('express').Router(),
	midd = require('../middlewares'),
	controller = require('../controllers/contactController');


r.route('/').get(midd.save_prevPath, controller.get_contact_page);;

module.exports = r;
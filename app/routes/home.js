var r = require('express').Router(),
	midd = require('../middlewares'),
	homeC = require('../controllers/homeController'),
	postC = require('../controllers/postController');


r.route('/').get(midd.save_prevPath, homeC.get_home_page);
r.route('/tim-kiem').get(midd.save_prevPath, postC.search_posts);

module.exports = r;
var r = require('express').Router(),
	midd = require('../middlewares'),
	adminC = require('../controllers/adminController'),
	postC = require('../controllers/postController');


r.get('/', adminC.dashboard);

r.route('/tao-bai-dang')
	.get(midd.save_prevPath, adminC.page_create_post)
	.post(midd.save_prevPath, postC.create_post);

module.exports = r;
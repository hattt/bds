var r = require('express').Router(),
	midd = require('../middlewares'),
	controller = require('../controllers/postController');


//?page=[]
r.get('/get-posts-by-category/:category', midd.get_category_info, controller.get_posts_by_category);
//?q=[]&page=[]
r.get('/get-posts-by-querystring', controller.get_posts_by_querystring);


//get comment
//query: parent, page
r.get('/get-comments/:postId', controller.get_comments);

//post comment
r.post('/post-comment/:postId', midd.need_login, controller.create_comment);


module.exports = r;
var r = require('express').Router();

var midd = require('../middlewares'),
	home = require('./home'),
	contact = require('./contact'),
	posts = require('./posts'),
	admin = require('./admin'),
	auth = require('./auth'),
	ajax = require('./ajax');

r.use('/', auth);
r.use(midd.layout_handle);
r.use('/', home);
r.use('/contact', contact);
r.use('/post', posts);
r.use('/admin', midd.need_admin, midd.hide_likeBox, admin);
r.use('/ajax', ajax);


r.use((req, res, next) => {
	next(new Error('404'));
})
r.use(midd.error_handle);

module.exports = r;
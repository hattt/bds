var r = require('express').Router(),
	passport = require('passport');


// =====================================
// LOCAL ROUTES =======================
// =====================================
r.post('/login', (req, res, next) => {
	passport.authenticate('local-login', function(err, user, info) {

		var data = {
			success: false,
			msg: info
		};

		if (err) {
			return next(err);
		}
		if (!user) {
			data.success = false;
			return res.json(data);
		}
		req.logIn(user, function(err) {
			if (err) {
				return next(err);
			}
			data.success = true;
			return res.json(data);
		});
	})(req, res, next);
});

r.post('/signup', (req, res, next) => {
	passport.authenticate('local-signup', function(err, user, info) {
		var data = {
			success: false,
			msg: info
		};

		if (err) {
			return next(err);
		}
		if (!user) {
			return res.json(data);
		}
		req.logIn(user, function(err) {
			if (err) {
				return next(err);
			}
			data.success = true;
			return res.json(data);
		});
	})(req, res, next);
});

// =====================================
// FACEBOOK ROUTES =======================
// =====================================
r.get('/auth/facebook', passport.authenticate('facebook', {
	callbackURL: '/auth/facebook/callback',
	scope: 'email'
}));
r.get('/auth/facebook/callback', (req, res, next) => {
	passport.authenticate('facebook', {
		successRedirect: req.session.prevPath || '/',
		failureRedirect: req.session.prevPath || '/'
	})(req, res, next);
});
r.get('/connect/facebook', passport.authorize('facebook', {
	callbackURL: '/connect/facebook/callback',
	scope: 'email'
}));
r.get('/connect/facebook/callback', (req, res, next) => {
	passport.authorize('facebook', {
		callbackURL: '/connect/facebook/callback',
		failureRedirect: req.session.prevPath || '/',
		successRedirect: req.session.prevPath || '/'
	});
}, (req, res, next) => {
	return res.redirect(req.session.prevPath || '/');
});

// =====================================
// GOOGLE ROUTES =======================
// =====================================
r.get('/auth/google', passport.authenticate('google', {
	callbackURL: '/auth/google/callback',
	scope: ['profile', 'email']
}));
r.get('/auth/google/callback', (req, res, next) => {
	passport.authenticate('google', {
		successRedirect: req.session.prevPath || '/',
		failureRedirect: req.session.prevPath || '/'
	})(req, res, next);
});
r.get('/connect/google', passport.authorize('google', {
	callbackURL: '/connect/google/callback',
	scope: ['profile', 'email']
}));
r.get('/connect/google/callback', (req, res, next) => {
	passport.authorize('google', {
		callbackURL: '/connect/google/callback',
		failureRedirect: req.session.prevPath || '/',
		successRedirect: req.session.prevPath || '/'
	});
}, (req, res, next) => {
	return res.redirect(req.session.prevPath || '/');
});

// =====================================
// TWITTER ROUTES =====================
// =====================================
r.get('/auth/twitter', passport.authenticate('twitter', {
	callbackURL: '/auth/twitter/callback'
}));
r.get('/auth/twitter/callback', (req, res, next) => {
	passport.authenticate('twitter', {
		successRedirect: req.session.prevPath || '/',
		failureRedirect: req.session.prevPath || '/'
	})(req, res, next);
});
r.get('/connect/twitter', passport.authorize('twitter', {
	callbackURL: '/connect/twitter/callback',
}));
r.get('/connect/twitter/callback', (req, res, next) => {
	passport.authorize('twitter', {
		callbackURL: '/connect/twitter/callback',
		successRedirect: req.session.prevPath || '/',
		failureRedirect: req.session.prevPath || '/'
	});
}, (req, res, next) => {
	return res.redirect(req.session.prevPath || '/');
});


// =====================================
// LOGOUT ==============================
// =====================================
r.get('/logout', function(req, res) {
	req.logout();
	res.redirect(req.session.prevPath || '/');
});

module.exports = r;
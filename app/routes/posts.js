var r = require('express').Router(),
	CONFIG = require('../../config'),
	midd = require('../middlewares'),
	controller = require('../controllers/postController');

var cats = CONFIG.POST_CATEGORIES;

r.get('/:category', midd.get_category_info, midd.save_prevPath, controller.list_posts);

r.get('/:category/:name', midd.get_category_info, midd.get_a_post_by_path, midd.save_prevPath, controller.a_post);

module.exports = r;
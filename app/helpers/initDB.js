var mongoose = require('mongoose'),
	q = require('q'),
	DATA = require('../../data'),
	User = require('../models/User'),
	Category = require('../models/Category'),
	Post = require('../models/Post'),
	Comment = require('../models/Comment');

var root_categories = DATA.Category.root_categories;
var child_categories = DATA.Category.child_categories;
var users = DATA.User.users;
var posts = DATA.Post.posts;;

mongoose.Promise = q.Promise;

function initDB() {
	var d = q.defer();
	var pRootCategories = [],
		pChildCategories = [],
		pUsers = [],
		pPosts = [];

	root_categories.forEach((cat) => {
		var doc = new Category(cat);
		pRootCategories.push(doc.save());
	});
	users.forEach((user) => {
		var doc = new User(user);
		doc.local.password = doc.generateHash(doc.local.password);
		pUsers.push(doc.save());
	});

	q.all(pRootCategories).then((cats) => {
		child_categories.forEach((cat) => {
			var doc = new Category(cat);
			doc.parent = cats[cat.root]._id;
			pChildCategories.push(doc.save());
		});
		return [cats, q.all(pChildCategories)];
	}).spread((root_cats, child_cats) => {
		return [root_cats, child_cats, q.all(pUsers)]
	}).spread((root_cats, child_cats, users) => {
		posts.forEach((post) => {
			var info = {
				title: post.title,
				content: post.content,
				shortDesc: post.shortDesc,
				image: post.image,
				author: users[post.user]._id,
				category: post.isRootCat ? root_cats[post.cat]._id : child_cats[post.cat]._id
			}
			pPosts.push(Post.createPost(info));
		});

		return q.all(pPosts);
	}).then((posts) => {
		d.resolve(true);
	}).catch((err) => {
		d.reject(err);
	});
	return d.promise;
}


module.exports = () => {
	var d = q.defer();
	Category.findOne({}).then((doc) => {
		if (!doc) {
			initDB().then((rlt) => {
				d.resolve(rlt);
			}).catch((err) => {
				return [err, q.all([Post.remove({}), Category.remove({}), User.remove({})])];
			}).then((err, temp) => {
				d.reject(err);
			}).catch((err) => {
				d.reject(err);
			});
		} else d.resolve(false);
	}).catch((err) => {
		d.reject(err);
	})

	return d.promise;
};
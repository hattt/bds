var _this = this;

exports.solveSpace = function(str) {
	var patt = /[ ]{2,}/g;
	str = str.trim();
	return str.replace(patt, " ");
};
//source: https://huyhungdinh.blogspot.com/2015/12/javascript-chuyen-tieng-viet-co-dau.html
exports.removeVietnameseSign = (str) => {
	str = str.toLowerCase();
	str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
	str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
	str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
	str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
	str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
	str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
	str = str.replace(/đ/g, "d");
	str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
	str = str.replace(/ + /g, " ");
	str = str.trim();
	return str;
};

exports.convertPostTitleAndIdToPath = (title, id) => {
	var str = _this.removeVietnameseSign(title);
	str = str.replace(/ /g, "-");
	str += ('-' + id);
	return str;

};
exports.getPostIdFromPath = (path) => {
	var arr = path.split('-');
	return arr[arr.length - 1];

};

exports.convertStringToWords = function(str) {
	var rlt = str.split(/[ ]{1,}/g);
	return rlt.filter(function(w) {
		return w != "";
	});
};
var mongoose = require('mongoose'),
	q = require('q'),
	CONFIG = require('../../config'),
	DATA = require('../../data'),
	User = require('../models/User'),
	Category = require('../models/Category'),
	Post = require('../models/Post'),
	Comment = require('../models/Comment');

mongoose.Promise = q.Promise;

//result: [{root: Category, childs: [Catgory]}]
module.exports = () => {
	var d = q.defer();
	var rlt = [];
	Category.find({
		parent: undefined
	}).then((root_cats) => {
		var promises = [];
		root_cats.forEach((cat) => {
			promises.push(Category.find({
				parent: cat._id
			}));
		})
		return [root_cats, q.all(promises)]
	}).spread((root_cats, arr_child_cats) => {
		root_cats.forEach((cat, i) => {
			rlt.push({
				parent: cat,
				childs: arr_child_cats[i]
			});
		});
		d.resolve(rlt);
	}).catch((err) => {
		d.reject(err);
	});
	return d.promise;
};
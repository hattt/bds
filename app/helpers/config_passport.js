var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var q = require('q');

require('mongoose').Promise = q.Promise;

var User = require('../models/User');

var CONFIG = require('../../config').AuthConfig;

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, done) {

            var user = req.user; //session user

            //local user
            User.findOne({
                'local.email': email
            }).then((xuser) => {

                //NOT YET LOGIN
                if (!req.user) {
                    //xuser exist --> fail create
                    if (xuser) {
                        return done(null, false, 'Email này đã được đăng kí!');
                    } else { // create the user

                        var newUser = new User();

                        // set the user's local credentials
                        newUser.local = {
                            name: req.body.name,
                            email: email,
                            password: newUser.generateHash(password),
                        };
                        newUser.save(function(err) {
                            if (err)
                                return done(err);
                            return done(null, newUser);
                        });
                    }
                } else { //LOGGED IN (--> CONNECT/AUTHORIZE)
                    if (xuser) //xuser exist -> fail create & connect
                        return done(null, false, 'Email này đã được sử dụng!');


                    // update the session user's local credentials
                    user.local = {
                        name: req.body.name,
                        email: email,
                        password: user.generateHash(password),
                    };
                    user.save(function(err) {
                        if (err)
                            return done(err);
                        return done(null, user);
                    });
                }
            }).catch((err) => {
                done(err);
            })


        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, done) {
            User.findOne({
                'local.email': email
            }).then((user) => {
                // user not exist
                if (!user)
                    return done(null, false, 'Tài khoản không đúng!');

                //password wrong
                if (!user.validPassword(password))
                    return done(null, false, 'Nhập sai mật khẩu!');
                //right -> return user
                return done(null, user);
            }).catch((err) => {
                done(err);
            });

        }));


    // =========================================================================
    // FACEBOOK ==================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

            clientID: CONFIG.facebookAuth.clientID,
            clientSecret: CONFIG.facebookAuth.clientSecret,
            callbackURL: CONFIG.facebookAuth.callbackURL,
            passReqToCallback: true,
            profileFields: ['id', 'email', 'name'],

        },

        function(req, token, refreshToken, profile, done) {

            process.nextTick(function() {
                var user = req.user; //session user

                //facebook user
                User.findOne({
                    'facebook.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.facebook = {
                                id: profile.id,
                                token: token,
                                name: profile.name.givenName + ' ' + profile.name.familyName,
                                emails: profile.emails ? profile.emails[0].value : undefined
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('Tài khoản Facebook ' + profile.name.givenName + ' ' + profile.name.familyName + ' đã kết nối với tài khoản khác! Vui lòng thoát và đăng nhập lại!'));
                        }

                        // update the session user's facebook credentials
                        user.facebook = {
                            id: profile.id,
                            token: token,
                            name: profile.name.givenName + ' ' + profile.name.familyName,
                            emails: profile.emails ? profile.emails[0].value : undefined
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

            clientID: CONFIG.googleAuth.clientID,
            clientSecret: CONFIG.googleAuth.clientSecret,
            callbackURL: CONFIG.googleAuth.callbackURL,
            passReqToCallback: true,
        },
        function(req, token, refreshToken, profile, done) {

            process.nextTick(function() {
                var user = req.user; //session user

                //google user
                User.findOne({
                    'google.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.google = {
                                id: profile.id,
                                token: token,
                                name: profile.displayName,
                                emails: profile.emails ? profile.emails[0].value : undefined
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('Tài khoản google ' + profile.emails[0].value + ' đã kết nối với tài khoản khác! Vui lòng thoát và đăng nhập lại!'));
                        }

                        // update the session users google credentials
                        user.google = {
                            id: profile.id,
                            token: token,
                            name: profile.displayName,
                            emails: profile.emails ? profile.emails[0].value : undefined
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

    // =========================================================================
    // TWITTER ==================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({

            consumerKey: CONFIG.twitterAuth.consumerKey,
            consumerSecret: CONFIG.twitterAuth.consumerSecret,
            callbackURL: CONFIG.twitterAuth.callbackURL,
            passReqToCallback: true,
        },
        function(req, token, refreshToken, profile, done) {

            process.nextTick(function() {
                var user = req.user; //session user

                //twitter user
                User.findOne({
                    'twitter.id': profile.id
                }).then((xuser) => {

                    //NOT YET LOGIN
                    if (!user) {

                        if (xuser)
                            return done(null, xuser); //xuser exist, return  xuser
                        else { //xuser not exist -> create new

                            var newUser = new User();

                            newUser.twitter = {
                                id: profile.id,
                                token: token,
                                username: profile.username,
                                displayName: profile.displayName
                            };

                            newUser.save(function(err) {
                                if (err) return done(err);
                                return done(null, newUser);
                            });
                        }
                    } else { //LOGGED IN ( --> CONNECT/AUTHORIZE)

                        //xuser exist -> false connect
                        if (xuser) {
                            return done(null, false, req.flash('Tài khoản tưitter ' + profile.username + ' đã kết nối với tài khoản khác! Vui lòng thoát và đăng nhập lại!'));
                        }

                        // update the session users twitter credentials
                        user.twitter = {
                            id: profile.id,
                            token: token,
                            username: profile.username,
                            displayName: profile.displayName
                        };
                        user.save(function(err) {
                            if (err) return done(err);
                            return done(null, user);
                        });
                    }
                }).catch((err) => {
                    done(err);
                });
            });

        }));

};
var StringFormat = require('../helpers/string_format'),
	q = require('q'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema;
mongoose.Promise = q.Promise;

var Category = require('./Category'),
	User = require('./User');

var NUM_POSTS_PER_PAGE = 2,
	limit = NUM_POSTS_PER_PAGE + 1;

var postSchema = Schema({
	title: {
		type: String,
		minlength: 3,
		maxlength: 100,
		require: true
	},
	formated_title: {
		type: String,
	},
	path: {
		type: String,
	},
	category: {
		type: Schema.Types.ObjectId,
		ref: 'Category'
	},
	shortDesc: {
		type: String,
		default: 'Short Description'
	},
	content: {
		type: String,
		required: true
	},
	author: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	createDate: {
		type: Date,
		default: Date.now
	},
	images: [String]
}, {
	bufferCommands: false
});

//format info to response
postSchema.methods.format = function() {
	return JSON.parse(JSON.stringify(this, ['_id', 'title', 'category', 'content',
		'author', 'createDate', 'images', 'comments'
	]));
};

//---------------------VALIDATE REF----------------------
//category validate
postSchema.path('category').validate(function(value, respond) {

	Category.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'category not exist');

//author validate
postSchema.path('author').validate(function(value, respond) {

	User.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'author not exist');

//---------------------STATICS METHOD----------------------
postSchema.statics.createPost = function(info) {
	//info: {title, content, shortDesc, category, author, image}
	var d = q.defer();

	info.images = ['http://via.placeholder.com/200x300'];
	info.images.push(info.image);
	var post = new this(info);

	post.save().then((post) => {
		post.path = StringFormat.convertPostTitleAndIdToPath(post.title, post.id);
		post.formated_title = StringFormat.removeVietnameseSign(post.title);
		return post.save();
	}).then((post) => {
		d.resolve(post);
	}).catch((err) => {
		d.reject(err);
	});

	return d.promise;
};
postSchema.statics.countPostsByCategory = function(_cat) {
	var d = q.defer();

	if (_cat.parent) {
		this.find({
			category: _cat._id
		}).then((posts) => {
			d.resolve(posts.length);
		}).catch((err) => {
			d.reject(err);
		});
	} else {
		Category.find({
			parent: _cat._id
		}).then((cats) => {
			cats.push(new Category(_cat));

			var conditions = [];
			cats.forEach((cat) => {
				conditions.push({
					category: cat._id
				});
			});
			return this.find({
				$or: conditions
			});
		}).then((posts) => {
			d.resolve(posts.length);
		}).catch((err) => {
			d.reject(err);
		})
	}

	return d.promise;
};
postSchema.statics.getAllPostsByCategory = function(_cat, page) {
	var d = q.defer();
	var page = (page < 0) ? 1 : page,

		skip = NUM_POSTS_PER_PAGE * (page - 1);

	if (_cat.parent) {
		this.find({
			category: _cat._id
		}).skip(skip).limit(limit).populate('category').then((posts) => {
			d.resolve(posts);
		}).catch((err) => {
			d.reject(err);
		});
	} else {
		Category.find({
			parent: _cat._id
		}).then((cats) => {
			cats.push(new Category(_cat));

			var conditions = [];
			cats.forEach((cat) => {
				conditions.push({
					category: cat._id
				});
			});
			return this.find({
				$or: conditions
			}).skip(skip).limit(limit).populate('category');
		}).then((posts) => {
			d.resolve(posts);
		}).catch((err) => {
			d.reject(err);
		});
	}

	return d.promise;
};
postSchema.statics.getAllPostsByCategory_SortByDate = function(_cat, page) {
	var d = q.defer();
	var page = (page < 0) ? 1 : page,
		skip = NUM_POSTS_PER_PAGE * (page - 1);
	if (_cat.parent) {
		this.find({
			category: _cat._id
		}).sort({
			createDate: -1
		}).skip(skip).limit(limit).populate('author', 'local.name').populate('category').then((posts) => {
			d.resolve(posts);
		}).catch((err) => {
			d.reject(err);
		});
	} else {
		Category.find({
			parent: _cat._id
		}).then((cats) => {
			cats.push(new Category(_cat));

			var conditions = [];
			cats.forEach((cat) => {
				conditions.push({
					category: cat._id
				});
			});
			return this.find({
				$or: conditions
			}).sort({
				createDate: -1
			}).skip(skip).limit(limit).populate('author', 'local.name').populate('category');
		}).then((posts) => {
			d.resolve(posts);
		}).catch((err) => {
			d.reject(err);
		})
	}

	return d.promise;
};

postSchema.statics.countPostsByQueryString = function(query_str) {
	var d = q.defer();

	query_str = StringFormat.removeVietnameseSign(query_str);
	var query_words = StringFormat.convertStringToWords(query_str);
	if (query_words.length == 0) d.resolve(0);

	var regStr = '';
	query_words.forEach((word) => {
		regStr += ('(?=.*' + word + ')');
	});

	this.find({
		formated_title: {
			$regex: new RegExp(regStr),
			$options: 'i'
		}
	}).then((posts) => {
		d.resolve(posts.length);
	}).catch((err) => {
		d.reject(err);
	})
	return d.promise;
};
postSchema.statics.searchPosts = function(query_str, page) {
	var d = q.defer();
	var page = (page < 0) ? 1 : page,

		skip = NUM_POSTS_PER_PAGE * (page - 1);

	query_str = StringFormat.removeVietnameseSign(query_str);
	var query_words = StringFormat.convertStringToWords(query_str);
	if (query_words.length == 0) d.resolve([]);

	var regStr = '';
	query_words.forEach((word) => {
		regStr += ('(?=.*' + word + ')');
	});

	this.find({
		formated_title: {
			$regex: new RegExp(regStr),
			$options: 'i'
		}
	}).sort({
		createDate: -1
	}).skip(skip).limit(limit).populate('category').then((posts) => {
		d.resolve(posts);
	}).catch((err) => {
		d.reject(err);
	});
	return d.promise;
};


module.exports = mongoose.model('Post', postSchema);
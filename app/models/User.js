var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    admin: {
        type: Boolean,
        default: false
    },
    local: {
        type: {
            name: String,
            email: String,
            password: String
        },
        required: false
    },
    facebook: {
        type: {
            id: String,
            token: String,
            email: String,
            name: String
        },
        required: false
    },
    twitter: {
        type: {
            id: String,
            token: String,
            displayName: String,
            username: String
        },
        required: false
    },
    google: {
        type: {
            id: String,
            token: String,
            email: String,
            name: String
        },
        required: false
    }

}, {
    bufferCommands: false
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};
//get name
userSchema.methods.getDisplayName = function() {
    var account = this.local || this.facebook || this.google || this.twitter;
    return account.name || account.displayName;
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
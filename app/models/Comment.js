var q = require('q'),
	mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	Post = require('./Post');
mongoose.Promise = q.Promise;

// var NUM_PER_PAGE = 100,
// 	limit = NUM_PER_PAGE + 1;
var commentSchema = Schema({
	post: {
		type: Schema.Types.ObjectId,
		ref: 'Post',
		required: true
	},
	text: {
		type: String,
		minlength: 3,
	},
	parent: {
		type: Schema.Types.ObjectId,
		ref: 'Comment'
	},
	by: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	at: {
		type: Date,
		default: Date.now
	}

}, {
	bufferCommands: false
});


//---------------------STATICS METHOD----------------------
commentSchema.statics.countComments = function(postId) {
	var d = q.defer();

	this.find({
		post: postId
	}).then((comments) => {
		d.resolve(comments.length);
	}).catch((err) => {
		d.reject(err);
	});

	return d.promise;
};
commentSchema.statics.getComments = function(postId, parentId, page) {
	var d = q.defer();
	// var page = (page < 0) ? 1 : page,

	// 	skip = NUM_PER_PAGE * (page - 1);
	this.find({
		post: postId,
		parent: parentId ? parentId : undefined
	}).populate('by').then((comments) => {
		var promises = [];
		comments.forEach((c) => {
			promises.push(Comment.find({
				parent: c._id
			}));
		});
		return [comments, q.all(promises)];
	}).spread((comments, rlt) => {
		var data = [];
		for (var i = 0; i < comments.length; i++) {
			var t = JSON.parse(JSON.stringify(comments[i]));
			t.by = comments[i].by.getDisplayName();
			t.num_replies = rlt[i].length;
			data.push(t);
		}

		d.resolve(data);
	}).catch((err) => {
		d.reject(err);
	});

	return d.promise;
};


var Comment = mongoose.model('Comment', commentSchema);

//---------------------VALIDATE REF----------------------
//parent validate
commentSchema.path('parent').validate(function(value, respond) {

	Comment.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'parent not exist');

//post validate
commentSchema.path('post').validate(function(value, respond) {

	Post.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'post not exist');
module.exports = Comment;
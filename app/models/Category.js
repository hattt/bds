var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var categorySchema = Schema({
	name: {
		type: String,
		minlength: 3,
		maxlength: 100,
		required: true
	},
	pathname: {
		type: String,
		match: /^[-0-9A-Za-z]{1,}$/,
		unique: true,
		required: true
	},
	description: {
		type: String,
		default: 'Category Description'
	},
	parent: {
		type: Schema.Types.ObjectId,
		ref: 'Category'
	}
});

var Category = mongoose.model('Category', categorySchema);

//parent validate
categorySchema.path('parent').validate(function(value, respond) {

	Category.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'parent not exist');

// //format info to response
// categorySchema.methods.format = function() {
// 	return JSON.parse(JSON.stringify(this, ['_id', 'name', 'description', 'posts']));
// };

// //update
// categorySchema.methods.update = function(data) {
// 	if (data.hasOwnProperty('name')) this.name = data.name;
// 	if (data.hasOwnProperty('description')) this.description = data.description;
// 	return true;
// };

module.exports = Category;
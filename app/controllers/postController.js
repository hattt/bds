var q = require('q'),
	CONFIG = require('../../config'),
	Post = require('../models/Post'),
	Comment = require('../models/Comment'),
	helpers = require('../helpers');



exports.list_posts = (req, res, next) => {
	var vm = {
		title: req.category.name,
		category: req.category,
		num_posts: 0
	};

	Post.countPostsByCategory(req.category).then((num) => {
		vm.num_posts = num;
		return res.render('post/list_posts', vm);
	}).catch((err) => {
		next(err);
	})
};
exports.search_posts = (req, res, next) => {
	var query_str = helpers.StringFormat.solveSpace(req.query.q);
	if (query_str === '') return res.redirect('/');

	var vm = {
		title: 'Tìm kiếm: \'' + query_str + '\'',
		num_posts: 0
	};

	Post.countPostsByQueryString(req.query.q).then((num) => {
		vm.num_posts = num;
		return res.render('post/list_posts', vm);
	}).catch((err) => {
		next(err);
	})
};
exports.a_post = (req, res, next) => {
	var vm = {
		post: req.post
	};

	Comment.countComments(req.post.id).then((num) => {
		vm.post.totalComments = num;
		res.render('post/detail_post', vm);
	}).catch((err) => {
		next(err);
	})
};

//JSON DATA
exports.create_post = (req, res, next) => {
	//req.body: {title, shortDesc, content, image} 
	var form = req.body;
	form.author = req.user.id;

	Post.createPost(form).then((post) => {
		res.json({
			success: true,
			msg: 'Tạo bài đăng thành công!'
		});
	}).catch((err) => {
		if (err.name == 'ValidationError') {
			res.json({
				success: false,
				msg: 'Thông tin không hợp lệ!'
			});
		} else next(err);
	});
};

exports.get_posts_by_category = (req, res, next) => {
	Post.getAllPostsByCategory_SortByDate(req.category, req.query.page).then((posts) => {
		var promise = [];
		posts.forEach((post) => {
			promise.push(Comment.countComments(post.id));
		});
		return [posts, q.all(promise)];
	}).spread((posts, counts) => {
		var data = [],
			temp;
		counts.forEach((count, i) => {
			temp = JSON.parse(JSON.stringify(posts[i]));
			temp.totalComments = count;
			data.push(temp);
		});
		return res.json({
			success: true,
			data: data
		});
	}).catch((err) => {
		res.json({
			success: false,
			msg: 'Có lỗi xảy ra!'
		});
	})
};
exports.get_posts_by_querystring = (req, res, next) => {
	Post.searchPosts(req.query.q, req.query.page).then((posts) => {
		var promise = [];
		posts.forEach((post) => {
			promise.push(Comment.countComments(post.id));
		});
		return [posts, q.all(promise)];
	}).spread((posts, counts) => {
		var data = [],
			temp;
		counts.forEach((count, i) => {
			temp = JSON.parse(JSON.stringify(posts[i]));
			temp.totalComments = count;
			data.push(temp);
		});
		return res.json({
			success: true,
			data: data
		});
	}).catch((err) => {
		res.json({
			success: false,
			msg: 'Có lỗi xảy ra!'
		});
	})
};

exports.create_comment = (req, res, next) => {
	var comment = new Comment({
		text: req.body.text,
		by: req.user.id,
		post: req.params.postId,
		parent: req.body.parent
	});

	comment.save().then((new_comment) => {
		new_comment = JSON.parse(JSON.stringify(new_comment));
		new_comment.by = req.user.getDisplayName();
		res.json({
			success: true,
			msg: 'Bình luận thành công!',
			data: new_comment
		});
	}).catch((err) => {
		if (err.name == 'ValidationError') {
			res.json({
				success: false,
				msg: 'Thông tin không hợp lệ!'
			});
		} else next(err);
	});
};
exports.get_comments = (req, res, next) => {
	Comment.getComments(req.params.postId, req.query.parent, req.query.page).then((comments) => {
		return res.json({
			success: true,
			data: comments
		});
	}).catch((err) => {
		res.json({
			success: false,
			msg: 'Có lỗi xảy ra!'
		});
	})
};
var CONFIG = require('../../config');



exports.dashboard = (req, res, next) => {
	var vm = {};
	return res.render('admin', vm);
};

exports.page_create_post = (req, res, next) => {
	var vm = {
		news_cat: req.APPDATA.news_cat
	};
	return res.render('admin/create-post', vm);
};